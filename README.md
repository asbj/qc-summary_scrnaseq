## Introduction

This repository contains R-code for creating some summary plots of QC-data from single-cell RNA-seq experiments. It will suggest filtering criteria for removing cells from the dataset based on some different filtering criteria. The criteria for filtering can be set in a configuration file (example in config.yaml). 

OBS! Some parts of the package is still under development...

## Dependencies

* Non R: pandoc - is normally installed with R-studio, but if you have a stand-alone version of R, make sure that you have pandoc installed.
* R-packages: rmarkdown, yaml, vioplot, gplots, scater, optparse

## Configuration file

The different columns in the QC-file that should be used for filtering out bad libraries has to be defined in the configuration yaml file. The default cutoff for all metrics is defined as 2 standard deviations from the mean, but you have to define if you should use the lower tail, upper tail or both ends of the distribution. And you can also set custom cutoffs in the configuration file.

The default setting is to remove any cell that fails for 2 of the tested QC statistics. This can be altered with the `nfail` parameter. 

You can also specify additional QC-metrics that should be plotted per batch under the section plot. 

More detailed instructions are included in the file `source/config.yaml`

## Running the code

Call to script is:

          Rscript source/render_qc_summary.R -i QC-data_file.txt -o outprefix -s sampleinfo_file.txt -c config.yaml 

Where required input are:

* One or several files with the QC-data that you wish to plot, such as mapping statistics, RSeQC output, detected molecules, spike-in ratios etc. If several files, use comma to separate them in the function call.
* A file with sample information, with information of different batches, different celltypes (if known) and any other information that you want to use to split the data.
* A configuration file (example file in config.yaml) that indicates which of the columns in the two files that you want to include.

Optional input arguments are:

--bulk_file 		- a file with samples that should not be included in the analysis, such as non-single cell data within the same file

--sep				- the separator that was used in the file, default is to use comma-separated files for -s & -i 

-e, --expression    - a file with counts,rpkms,tpms or similar - OBS! not implemented yet


## Creating reports from SS2 data delivered by the Scilifelab ESCG facility

For data delivered by the Eukaryotic Single Cell Genomics Facility at Scilifelab, there is a python script for parsing out all relevant information needed to create a qc-report from their data. The script you need is `source/ESCG_SS2/extract_qc_stats.py` that requires python3 with pandas installed. 

To create all the qc-files in the format you need, just run:

        python source/ESCG_SS2/extract_qc_stats.py -i /proj/bXXXXXXX/INBOX/SS2_17_153 /proj/bXXXXXXX/INBOX/SS2_17_154 -o /proj/bXXXXXXX/nobackup/private/qc_summary

Specify all input folders with flag `-i`. OBS! You first need to extract the expression files in the expression.tar.gz file of each folder with command like:

	tar -zvxf SS2_17_154_expression.tar.gz

Flag `-o` specifies the path to an output folder. 

The script produces 3 files:

* rpkms_all_plates.csv - expression matrix with duplicate genes removed in csv format
* qc_stats_all_plates.csv - all qc-stats
* metadata_all_plates.csv - a metadata table for all the samples. 

The automatically created metadata table only contains information on plate, row and column. If you want to add in more information on batches, celltypes etc to include in the qc summary, you can edit that file and add in extra columns. To include them in the report you need to edit the configuration file `source/ESCG_SS2/config_escg_ss2.yaml` under section `batches`.

Once you have all the files in your output folder. You can creat a qc-report with:

     Rscript source/render_qc_summary.R -i  /proj/bXXXXXXX/nobackup/private/qc_summary/qc_stats_all_plates.csv -o /proj/bXXXXXXX/nobackup/private/qc_summary/qc_summary -c source/ESCG_SS2/config_escg_ss2.yaml -s  /proj/bXXXXXXX/nobackup/private/qc_summary/metadata_all_plates_edited.csv -e /proj/bXXXXXXX/nobackup/private/qc_summary/rpkms_all_plates.csv


## Questions
Please contact asa.bjorklund@scilifelab.se if you have any questions.

