#!/usr/bin/env python3

"""
Will take the delivery folders from the Scilifelab Eukaryotic Single Cell Genomics Platform for Smart-Seq2 data and
parse out relevant information for creating a qc-summary.

OBS! requires pandas python package

Written by Asa Bjorklund 2017
"""
# standard python libraries
import os, sys, re
import pandas as pd
import numpy as np
from argparse import ArgumentParser
parser = ArgumentParser(description='Extract qc-stats from SS2 data delivered by the ESCG platform')

# [Required input]
parser.add_argument('-i', '--indirs', metavar='indirs', nargs='+', help="Input directories, all folders delivered by the ESCG, make sure to have the expression data unzipped firest", required=True)
parser.add_argument('-o', '--outdir', metavar='outdir', help='Output directory for qc-stats', required=True)
parser.add_argument('--spike_in',metavar='spike_in', help="String pattern for identifying the spike-in molecules",default="ERCC-0")
args = parser.parse_args()

QC = pd.DataFrame()
RPKM = pd.DataFrame()

for dir in args.indirs:
    plate_id = os.path.split(dir)[-1]
    qc = pd.read_csv(os.path.join(dir,"%s_qc.txt"%plate_id),sep="\t",index_col=1)
    rpkm = pd.read_csv(os.path.join(dir,"rpkms.tab"),sep="\t",index_col=0)
    ercc = pd.read_csv(os.path.join(dir,"rpkms-ercc.tab"),sep="\t",index_col=0)
    
    qc["ercc_detection"]=  ercc[ercc > 1.0].count()
    sum_rpkm = rpkm.sum(skipna=True,axis=0)
    sum_ercc = ercc.sum(skipna=True,axis=0)
    qc["ercc_ratio"] = sum_ercc/(sum_ercc + sum_rpkm)
    
    # make new cell names that contains plate id
    qc.index = [plate_id + "_" + x for x in list(qc.index)]
    
    QC = QC.append(qc)
    
    # also change names for RPKM columns
    rpkm.columns = [plate_id + "_" + x for x in list(rpkm.columns)]
    RPKM = pd.concat([RPKM,rpkm],axis=1)


# write to files
# fix duplicate gene names in rpkm-matrix - add up the values
RPKM=RPKM.groupby(RPKM.index).sum()
RPKM.to_csv(os.path.join(args.outdir,"rpkms_all_plates.csv"), sep=",")

# add SM column first in df
colnames = ['SM'] + list(QC.columns)
QC['SM']=QC.index
QC = QC[colnames]
QC.to_csv(os.path.join(args.outdir,"qc_stats_all_plates.csv"), sep=",",index=False)


# also make one metadata table
well = [x.split("_")[-1]   for x in list(QC.index)]
column = [re.findall("\d+",x)[0] for x in well]
row = [re.findall("[A-Z]+",x)[0] for x in well]
META = pd.DataFrame({'SM':QC.index, 'plate':QC['experiment'],'column':column,'row':row})
META.to_csv(os.path.join(args.outdir,"metadata_all_plates.csv"), sep=",",index=False)


